FROM alpine:3.16.2 as build

WORKDIR /app/

RUN apk add --no-cache alpine-sdk=1.0 \
        cmake=3.24.1 \
        ibc-dev=0.7.2 readline=8.1.2 \
        readline-dev=6.3.008 gcc=12.1.1 make=4.3 wget=1.18 \
        lua5.3=5.3.6 \
        lua5.3-dev=5.3.6 \
        luarocks=2.4.4 && \
        luarocks-5.3 install --tree /app luacheck && \
        luarocks-5.3 install --tree /app luacheck-formatter-sonar

FROM pipelinecomponents/base-entrypoint:0.5.0 as entrypoint

FROM alpine:3.16
COPY --from=entrypoint /entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
ENV DEFAULTCMD luacheck

# hadolint ignore=DL3018
RUN apk add --no-cache lua5.3

COPY --from=build /app/ /app/

ENV PATH "$PATH:/app/bin/"

# Lua paths to check /app
ENV LUA_PATH='/app/share/lua/5.3/?.lua;/app/share/lua/5.3/?/init.lua;/root/.luarocks/share/lua/5.3/?.lua;/root/.luarocks/share/lua/5.3/?/init.lua;/usr/share/lua/5.3/?.lua;/usr/share/lua/5.3/?/init.lua;/usr/share/lua/common/?.lua;/usr/share/lua/common/?/init.lua;/usr/local/share/lua/5.3/?.lua;/usr/local/share/lua/5.3/?/init.lua;/usr/local/lib/lua/5.3/?.lua;/usr/local/lib/lua/5.3/?/init.lua;/usr/lib/lua/5.3/?.lua;/usr/lib/lua/5.3/?/init.lua;./?.lua;./?/init.lua'
ENV LUA_CPATH='/app/lib/lua/5.3/?.so;/root/.luarocks/lib/lua/5.3/?.so;/usr/lib/lua/5.3/?.so;/usr/local/lib/lua/5.3/?.so;/usr/local/lib/lua/5.3/loadall.so;/usr/lib/lua/5.3/loadall.so;./?.so'


WORKDIR /code/

# Build arguments
ARG BUILD_DATE
ARG BUILD_REF

# Labels
LABEL \
    maintainer="Robbert Müller <dev@pipeline-components.dev>" \
    org.label-schema.description="Luacheck in a container for gitlab-ci" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.name="Luacheck" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://pipeline-components.gitlab.io/" \
    org.label-schema.usage="https://gitlab.com/pipeline-components/luacheck/blob/master/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/pipeline-components/luacheck/" \
    org.label-schema.vendor="Pipeline Components"
